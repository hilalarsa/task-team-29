# task-team-29

# Intro 
Repository ini bertujuan untuk mengumpulkan progress task tiap student, serta membiasakan dengan git flow menggunakan multiple branch dan Merge request creation.
Dari repository, student aku harapkan bisa aktif untuk *push* hasil pengerjaan dari task masing-masing, kedalam repository ini. Hasil tiap student *tidak* mempengaruhi penilaian chapter, maupun terhadap kelas binar secara keseluruhan.

# Step to contribute
1. Clone repository ini di local, gunakan ```git clone```
2. Buat sebuah branch baru di local, dengan nama student masing-masing sebagai branch. Gunakan perintah ```git checkout -b <nama-branch>``` Contoh: git checkout -b hilal-arsa
3. Buat sebuah folder baru, dengan format `nama-student`. Pastikan lokasi folder ini sejajar dengan README.md (di root project).
4. Student bebas mengisi isi folder ini dengan task yang telah dikerjakan. Jika mau, bisa membuat subfolder tambahan didalamnya. Constoh direktori dengan subfolder : task-team-29/hilal-arsa/chapter-5, lalu didalamnya diisi file-file sesuai task yang dikerjakan. Pembuatan direktori bisa menggunakan hasil jadinya sebagai contoh, di branch *hilal-arsa* yang bisa diakses di dashboard gitlab, via klik dropdown nama branch.
5. Tambahkan file .gitignore, dan isi dengan ```node_modules``` (contoh di branch hilal-arsa), untuk meng-exclude node_modules dari git.
5. Setelah folder masing-masing terisi, lakukan tahap push ke remote, dengan :
- ```git add .```
- ```git commit -m <message>```
- ```git push origin <branch-name>```
6. Jika ingin melihat hasil pekerjaan masing-masing, bisa diakses melalui gitlab dasboard, dan ganti nama branch sesuai nama yang dibuat di local

# Target
1. Nantinya, branch-branch yang telah dibuat, akan kita coba demokan untuk merge request ke main branch. Dari sini kita bisa demokan juga saat terjadi conflict dalam git dengan code student yang lain.

# To navigate
1. ```git fetch --all``` untuk mendapatkan list of all branch yang ada di remote + mengupdate branch local dengan latest code dari remote repository
2. ```git branch``` untuk melihat list branch yang ada di local

Good luck!