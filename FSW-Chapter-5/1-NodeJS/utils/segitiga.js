// > Membuat Local Module 
const luasSegitiga = (alas, tinggi) => {
  return alas * tinggi / 2;
}

// > Export Module 
module.exports = { luasSegitiga };