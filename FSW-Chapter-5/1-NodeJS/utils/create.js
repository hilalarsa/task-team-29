const fs = require('fs');

const createPerson = (person) => {
  fs.writeFileSync('./data/person.json', JSON.stringify(person, null, 2));
  return person;
};

module.exports = { createPerson };