const luasLingkaran = (r) => 3.14 * r * r;

const kelilingLingkaran = (r) => 2 * 3.14 * r;

module.exports = {
  luasLingkaran,
  kelilingLingkaran
};