// > Import Core Module 
const os = require('os');
const fs = require('fs');
// > Import Third Party Module 
const chalk = require('chalk');
// > Import Local Module 
const { luasLingkaran, kelilingLingkaran } = require('./utils/lingkaran');

// > Use Core Module os
// console.info(`Free Memory: ${os.freemem}`);
// console.info(`Hostname: ${os.hostname}`);

// ======================================================================

// > Use Third Party Module (NPM Module): Chalk 
// console.info(chalk.red.bgGreen('Hello World'));

// ======================================================================

// > Use Local Module 
// console.info(`Luas Lingkaran: ${luasLingkaran(6)}`);
// console.info(`Keliling Lingkaran: ${kelilingLingkaran(12)}`);

// ======================================================================

// > Use Core Module fs.readFileSync & fs.readFile

// => fs.readFileSync (Proses Dilakukan Secara Synchronous)
// console.info('Mulai');
// const readFileSync = fs.readFileSync('./data/hello.txt', 'utf-8');
// console.info(readFileSync);
// console.info('Selesai');
/*
  # Output:
  Mulai
  hello nama saya adrian mulyawan
  kalian bisa memanggil saya wawan
  Selesai
*/

// => fs.readFile (Proses Dilakukan Secara Asyncrhonous)
// console.info('Mulai');
// fs.readFile('./data/hello.txt', 'utf-8', (err, data) => {
//   if(err) console.info(err);
//   console.info(data);
// })
// console.info('Selesai');
/*
  # Output:
  Mulai
  Selesai
  hello nama saya adrian mulyawan
  kalian bisa memanggil saya wawan
*/

// ======================================================================

// const employees = require('./data/pegawai.json');

// 1. Masukan data employee kedalam file data/employess.json
// let data = {
//   "employees": [
//     {
//       "firstName":"John", 
//       "lastName":"Doe"
//     },
//     {
//       "firstName":"Anna",
//       "lastName":"Smith"
//     },
//     {
//       "firstName":"Peter", 
//       "lastName":"Jones"
//     }
//   ]
// };

// fs.writeFileSync('./data/pegawai.json', JSON.stringify(data, null, 2));

// 2. Tampilkan JSON file dengan readFile 
// fs.readFile('./data/pegawai.json', 'utf-8', (err, data) => {
//   if(err) console.info(err);
//   console.info(data);
// });

// const employees = require('./data/pegawai.json');

// 3. Import file JSON
// console.info(employees);

// 4. Akses salah satu object
// console.info(employees.employees[1].firstName);

// ======================================================================

// > Practice Lodash 
// const _ = require('lodash');

// 1.) Cek apakah sebuah nilai string / number
// const number1 = 120;
// const string1 = 'Binar Academy';

// console.info(`${number1} adalah string: ${_.isString(number1)}`);
// console.info(`${number1} adalah number: ${_.isNumber(number1)}`);

// console.info(`${string1} adalah string: ${_.isString(string1)}`);
// console.info(`${string1} adalah number: ${_.isNumber(string1)}`);

// 2.) Membuat camelCase dan Capitalize 
// const valueString1 = 'binar academy';
// const valueString2 = 'binar'

// console.info(`Bentuk camelCase Dari ${valueString1}: ${_.camelCase(valueString1)}`);
// console.info(`Bentuk Capitalize Dari ${valueString2}: ${_.capitalize(valueString2)}`);

// 3.) Mencari Value Dalam Array 
// const myArray = [1, 3, 5, 2, 4];
// console.info(`Nilai Array Index Pertama: ${_.first(myArray)}`);
// console.info(`Nilai Array Index Terakhir: ${_.last(myArray)}`);
// console.info(`Nilai ${myArray[3]} Berada Pada Index: ${_.indexOf(myArray, 2)}`);

// 4.) Sort Array 
// console.info(`Hasil Pengurutan Array Terkecil ke Terbesar: ${_.sortBy(myArray)}`);

// ======================================================================

// > Practice NPM Package: Moments 
const moment = require('moment');
moment.locale('id');

// 1. Mencetak Tanggal Hari Ini (DD/MM/YY)
const thisDay1 = moment().format('DD/MM/YY');
console.info(`Tanggal Hari Ini: ${thisDay1}`);

// 2. Mencetak Tanggal Hari Ini Dengan Format (09 Januari 2023)
const thisDay2 = moment().format('DD MMMM YYYY');
console.info(`Tanggal Hari Ini: ${thisDay2}`);

// 3. Mencetak Tanggal dan Waktu Hari Ini Dengan Fomrat (09 Januari 2023 19:00:00)
const thisDay3 = moment().format('DD MMMM YYYY, HH:mm:ss');
console.info(`Tanggal Hari Ini Beserta Jam: ${thisDay3}`);

// 4. Mencetak Selisih Hari Antara Hari Ini, Dengan Waktu Tertentu (Misal, 01/01/2000)
const yearDifference = moment("20000101", "YYYYMMDD").startOf('day').fromNow(); 
const dayDifference = moment("20230109", "YYYYMMDD").diff("20000101"); 
console.info(`Jarak Perbedaan Tahun Dari Tanggal 01 Januari 2000, Sampai Hari Ini Adalah: ${yearDifference}`);