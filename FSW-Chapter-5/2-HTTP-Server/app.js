const http = require('http');
const fs = require('fs');
const moment = require('moment');

const host = '127.0.0.1';
const port = 3000;

const thisDay = moment().format('MMMM Do YYYY, h:mm:ss a');
const htmlFile = `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Test</title>
</head>
<body>
  <h1>Tanggal Hari Ini: ${thisDay}</h1>
</body>
</html>`;

const saveHTML = fs.writeFileSync('./data/index.html', htmlFile);

const readJson = fs.readFileSync('./data/data.json', 'utf-8', (err, data) => {
  if (err) {
    console.info(err);
  }
  console.info(data);
});

const server = http.createServer((req, res) => {
  const url = req.url;

  if (url === '/') {
    res.writeHead(200, {
      'Content-Type': 'text/html'
    });
    const readFile = fs.readFileSync('./data/index.html', 'utf-8', (err, data) => {
      if (err) {
        console.info(err);
        console.info(data);
      }
    });
    res.write(readFile);
    res.end();
  } else if(url === '/data') {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.write(readJson);
    res.end();
  }
});

server.listen(port, () => {
  console.info(`HTTP server run in http://${host}:${port}`);
})