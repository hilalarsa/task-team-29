const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

// > Send Text
app.get('/send', (req, res) => {
  res.status(200).send('<h1>Home</h1>');
});

// > Send JSON
app.get('/json', (req, res) => {
  const data = [
    {
      id: 1,
      nama: 'Xiaomi 12 Pro',
      harga: 12000000
    },
    {
      id: 2,
      nama: 'IPhone 14 Pro Max',
      harga: 18000000
    }
  ];

  res.status(200).json({
    url: req.url,
    status: 200,
    message: 'Show All Data',
    data: data
  });
});

// > Send File
app.get('/sendFile', (req, res) => {
  // res.sendFile('index.html', { root: path.join(__dirname, '/pages/') });
  res.status(200).sendFile('binar_logo.jpg', { root: path.join(__dirname, '/img/') });
});

// > Send Params
app.get('/params/:id', (req, res) => {
  const id = req.params.id;
  res.status(200).send(`Anda mengirim params: ${id}`);
});

// > Send Query
app.get('/query', (req, res) => {
  res.status(200).json({
    status: 200,
    query: req.query
  });
});

// > Send Regex
app.get('/ab*cd', (req, res) => {
  res.status(200).send('ab*cd');
})

// > Jika tidak ditemukan routenya
app.get('*', (req, res) => {
  res.status(404).send('<h1>404 Not Found</h1>');
});

app.listen(port, () => {
  console.info(`Express run in port ${port}`);
})